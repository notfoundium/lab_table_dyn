#include "Table.hpp"

#include <iostream>

int		main(void) {
	Table table;

	std::cout << "is empty? " << table.isEmpty() << std::endl;

	std::cout << "<<Adding new elements>>" << std::endl;

	table.pushback(42, "Vlad");
	table.pushfront(21, "Max");
	table.pushback(84, "T");

	std::cout << "is empty? " << table.isEmpty() << std::endl;

	std::cout << table.find(42) << std::endl;
	std::cout << table.find(21) << std::endl;
	std::cout << table.find(84) << std::endl;
	table.remove(84);
	std::cout << "<<removed>>" << std::endl;
	std::cout << table.find(84) << std::endl;
	return (0);
}
