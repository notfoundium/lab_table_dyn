#ifndef TABLE_CLASS_HPP
# define TABLE_CLASS_HPP

# include <string>
# include <iostream>

class	Table {
	public:
		Table(void);
		~Table(void);

		bool		isEmpty(void) const;
		void		pushback(int key, std::string const& item);
		void		pushfront(int key, std::string const& item);
		std::string	find(int key) const;
		bool		remove(int key);

		struct Node {
			Node(int key, std::string value);
			~Node(void) {};

			int			key;
			std::string	value;
			Node*		next;
		};

	private:

		Node*	list;
};

std::ostream &operator<<(std::ostream& out, Table const& table);

#endif /* TABLE_CLASS_HPP */
