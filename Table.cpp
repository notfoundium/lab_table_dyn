#include "Table.hpp"

# include <stdexcept>

Table::Table(void) : list(NULL) {
	return ;
}

Table::~Table(void) {
	Node*	tmp;

	while (list) {
		tmp = list;
		list = list->next;
		delete tmp;
	}
	return ;
}

bool	Table::isEmpty(void) const {
	if (!list)
		return (true);
	else
		return (false);
}

void	Table::pushback(int key, std::string const& item) {
	Node*	current = this->list;

	if (!list) {
		list = new Node(key, item);
		return ;
	}
	while (current->next) {
		if (current->key == key)
			throw std::logic_error("Key is not unique");
		current = current->next;
	}
	current->next = new Node(key, item);
	return ;
}

void	Table::pushfront(int key, std::string const& item) {
	Node	*old = list;
	Node	*current = list;

	while (current->next) {
		if (current->key == key)
			throw std::logic_error("Key is not unique");
		current = current->next;
	}
	list = new Node(key, item);
	list->next = old;
	return ;
}

bool	Table::remove(int key) {
	Node*	prev;
	Node*	current = list;
	Node*	tmp;

	if (current->key == key) {
		tmp = list;
		list = current->next;
		delete (tmp);
		return (true);
	}
	prev = current;
	current = current->next;
	while (current) {
		if (current->key == key) {
			prev->next = current->next;
			delete (current);
			return (true);
		}
		prev = current;
		current = current->next;
	}
	return (false);
}

std::string	Table::find(int key) const {
	Node*	current = list;

	while (current) {
		if (current->key == key)
			return (current->value);
		current = current->next;
	}
	return ("NULL");
}

Table::Node::Node(int key, std::string value) :
	key(key),
	value(value),
	next(nullptr) {
	return ;
}
